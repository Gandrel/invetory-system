#ifndef VIEWWINDOW_H
#define VIEWWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QtSql>

#include <additemdialog.h>
#include <login.h>
#include <updateitem.h>

namespace Ui {
class ViewWindow;
}

class ViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewWindow(QWidget *parent = nullptr);
    ~ViewWindow();

private slots:

    void on_actionExit_triggered();

    void loadTablesList();

    void loadTable();

    void addItem(QString);

    void updateItem(QString);

    void deleteItem();

    void toggleDeleteBtns();

    void on_btnAdd_item_clicked();

    void on_btnDelete_item_clicked();

    void on_actionLog_in_triggered();

    void on_btnUpdate_item_clicked();


private:
    Ui::ViewWindow *ui;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QString dbPath = "classrooms.db";

    LogIn *lw;
    AddItemDialog *a;
    UpdateItem *uw;
};

#endif // VIEWWINDOW_H
