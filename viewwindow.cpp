#include "viewwindow.h"
#include "ui_viewwindow.h"

ViewWindow::ViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewWindow)
{
    ui->setupUi(this);
    loadTablesList();
    loadTable();
    connect(ui->cbxTable, SIGNAL(currentTextChanged(QString)), this, SLOT(loadTable()));
    connect(ui->lsvwTable, SIGNAL(clicked(QModelIndex)), this, SLOT(toggleDeleteBtns()));
}

ViewWindow::~ViewWindow()
{
    delete ui;
}

void ViewWindow::loadTablesList() {
    db.setDatabaseName(dbPath);
    db.open();

    QSqlQueryModel *qry_model = new QSqlQueryModel();
    qry_model->setQuery(db.exec("SELECT DISTINCT tbl_name FROM sqlite_master;"));
    ui->cbxTable->setModel(qry_model);

    db.close();
}

void ViewWindow::loadTable() {
    db.open();

    QModelIndex index = ui->lsvwTable->currentIndex();
    QString itemText = index.data(Qt::DisplayRole).toString();
    QSqlQueryModel *ls_model = new QSqlQueryModel();
    QSqlQueryModel *qn_mod = new QSqlQueryModel;
    ls_model->setQuery(db.exec("SELECT ItemName FROM " + ui->cbxTable->currentText()));
    qn_mod->setQuery(db.exec("SELECT Quantity FROM " + ui->cbxTable->currentText() + " WHERE ItemName = '" + itemText + "'"));
    qDebug () << "SELECT Quantity FROM " + ui->cbxTable->currentText() + " WHERE ItemName = '" + itemText + "'";
    ui->lsvwTable->setModel(ls_model);
    ui->lsvwQnt->setModel(qn_mod);

    db.close();
}

void ViewWindow::toggleDeleteBtns() {
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString itemText = index.data(Qt::DisplayRole).toString();
    qDebug() << "Selected item:" << itemText << "at index" << index.row();
    ui->btnDelete_item->setEnabled(true);
    ui->btnUpdate_item->setEnabled(true);

}


void ViewWindow::addItem(QString item_name) {
    db.open();

    QString item_id = QString::number(ui->lsvwTable->model()->rowCount());
    qDebug() << "SQL Command: INSERT INTO " + ui->cbxTable->currentText() + " VALUES ('" + item_name + "', 0)";
    db.exec("INSERT INTO " + ui->cbxTable->currentText() + " VALUES ('" + item_name + "', 0)");
    loadTable();

    db.close();
}

void ViewWindow::deleteItem() {
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString id = index.data(Qt::DisplayRole).toString();
    qDebug() << "SQL Command: DELETE  FROM " + ui->cbxTable->currentText() + " WHERE ItemName = '" + id + "'";

    db.open();
    db.exec("DELETE FROM " + ui->cbxTable->currentText() + " WHERE ItemName = '" + id + "'");
    db.close();

    loadTable();
}

void ViewWindow::updateItem(QString Item_name)
{
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString itemText = index.data(Qt::DisplayRole).toString();
    QString id = QString::number(index.row());
    qDebug() << "SQL Command: UPDATE " + ui->cbxTable->currentText() + "SET ItemName = " + Item_name + "WHERE ItemName =" + itemText;

    db.open();
    db.exec("UPDATE " + ui->cbxTable->currentText() + "SET ItemName = " + Item_name + "WHERE ItemNAme =" + itemText);
    db.close();

    loadTable();
}

void ViewWindow::on_actionExit_triggered()
{
    this->close();
}

void ViewWindow::on_btnAdd_item_clicked()
{
    a = new AddItemDialog(this);
    a->setModal(true);
    a->show();
    connect(a, SIGNAL(addItem(QString)), this, SLOT(addItem(QString)));
}

void ViewWindow::on_btnDelete_item_clicked()
{
    deleteItem();
}

void ViewWindow::on_actionLog_in_triggered()
{
   lw = new LogIn(this);
   lw->setModal(true);
   lw->show();
}

void ViewWindow::on_btnUpdate_item_clicked()
{
   uw = new UpdateItem(this);
   uw->setModal(true);
   uw->show();
   connect(uw, SIGNAL(updateItem(Qstring)), this, SLOT(updateItem(Qstring)));
}

