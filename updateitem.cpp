#include "updateitem.h"
#include "ui_updateitem.h"

UpdateItem::UpdateItem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateItem)
{
    ui->setupUi(this);
}

UpdateItem::~UpdateItem()
{
    delete ui;
}


void UpdateItem::on_OkCancel_accepted()
{
    if (!ui->lineEdit_ItemName->text().isEmpty()){
        emit updateItem(ui->lineEdit_ItemName->text());
    }
}
